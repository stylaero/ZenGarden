﻿using UnityEngine;
using System.Collections;

public class KillLeafs : MonoBehaviour {

  private LeafSpawner leafSpawner;

	void Start () {
    leafSpawner = FindObjectOfType<LeafSpawner>();
	
	}
  void OnTriggerExit(Collider other) {
    if (other.CompareTag(Constants.LeafTag)) {
      leafSpawner.RecycleLeaf(other.gameObject);
    }
  }
}
