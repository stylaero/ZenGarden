﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeafPile : MonoBehaviour, IPuzzle {

  public float TimeUntilSolved;
  private float solveTimer;

  public int ContainsLeafs;
  public Image image;
  public bool ShouldFill;
  public int LeafGoal;
  public float lerpSpeed;

  private float fillLerp;
  private bool _solved;

  public bool Solved() {
    return _solved;
  }

  public event PuzzleSolved OnPuzzleSolved;

  void OnEnable() {
    ContainsLeafs = 0;
    _solved = false;
    solveTimer = 0;
  }

  void Update () {
    float fill;
    if (ShouldFill)
      fill = ContainsLeafs/((float)LeafGoal);
    else
      fill = 1 - (ContainsLeafs - LeafGoal)/((float)LeafGoal);
    fill = Mathf.Clamp(fill, 0, 1);

    fillLerp = Mathf.Lerp(fillLerp, fill, Time.deltaTime * lerpSpeed);
    image.fillAmount = fillLerp;
    CheckFinished();
  }

  private void CheckFinished() {
    bool succeed;

    if (ShouldFill)
      succeed = ContainsLeafs >= LeafGoal;
    else
      succeed = ContainsLeafs <= LeafGoal;

    _solved = false;

    if (succeed) {
      solveTimer += Time.deltaTime;
      if (solveTimer >= TimeUntilSolved) {
      _solved = true;
      if (OnPuzzleSolved != null)
        OnPuzzleSolved();
      }
    } else {
      solveTimer = 0;
    }
  }

  void OnTriggerEnter(Collider other) {
    if (gameObject.activeInHierarchy) {
      if (other.CompareTag(Constants.LeafTag)) {
        ContainsLeafs++;
      }
    }
  }

  void OnTriggerExit(Collider other) {
    if (gameObject.activeInHierarchy) {
      if (other.CompareTag(Constants.LeafTag)) {
        ContainsLeafs--;
      }
    }
  }
}