﻿using UnityEngine;
using UnityEngine.UI;

public class GUIButton : MonoBehaviour, IPuzzle {
  public Color ActiveColor;
  public Color ActivatedColor;
  public Color InactiveColor;

  public Image ButtonImage;
  public Image SliderImage;

  public float ActivateTime;
  public float ActivateTrigger;
  private float activeTimer;

  private bool Activated;
  private bool selected;

  public event PuzzleSolved OnPuzzleSolved;

  private void UpdateGraphic() {
    if (Activated)
      ButtonImage.color = ActivatedColor;
    else if (selected)
      ButtonImage.color = ActiveColor;
    else
      ButtonImage.color = InactiveColor;

    SliderImage.fillAmount = activeTimer / (float)ActivateTime;
  }

	void FixedUpdate () {
    if (selected) {
      activeTimer += Time.fixedDeltaTime;
    } else {
      activeTimer -= Time.fixedDeltaTime;
    }
    activeTimer = Mathf.Clamp(activeTimer, 0, ActivateTime);

    if (activeTimer > ActivateTrigger) {
      if (!Activated && OnPuzzleSolved != null) {
        Activated = true;
        OnPuzzleSolved();
      }
    } else {
      Activated = false;
    }

    UpdateGraphic();
  }

  void OnTriggerEnter(Collider other) {
    if (gameObject.activeInHierarchy) {
      if (other.CompareTag(Constants.CursorTag)) {
        selected = true;
      }
  }
  }

  void OnTriggerExit(Collider other) {
    if (gameObject.activeInHierarchy) {
      if (other.CompareTag(Constants.CursorTag)) {
        selected = false;
      }
    }
  }

  public bool Solved() {
    return Activated;
  }
}