﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LeafSpawner : MonoBehaviour {

  private List<GameObject> leafs = new List<GameObject>();

  public GameObject LeafPrefab;
  public float Range;
  public int Amount;

	void Start () {
    for(int i = 0; i< Amount; i++) {
      GameObject leaf = Instantiate(LeafPrefab, GetSpawnPosition(), Random.rotation) as GameObject;
      leaf.transform.parent = transform;
      leafs.Add(leaf);
    }
	}

  public Vector3 GetSpawnPosition() {
      return Random.insideUnitSphere * Range + transform.position;
  }

  public void RecycleLeaf(GameObject leaf) {
    leaf.gameObject.SetActive(true);
    leaf.transform.position = GetSpawnPosition();
    Rigidbody r = leaf.GetComponent<Rigidbody>();
    if (r)
      r.velocity = Vector3.zero;
  }

  public void EnableLeafs() {
    foreach (var l in leafs)
      RecycleLeaf(l);
  }

  public void DisableLeafs() {
    foreach (var l in leafs)
      l.gameObject.SetActive(false);
  }
}
