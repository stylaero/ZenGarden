﻿using UnityEngine;
using System.Collections;

public class DisplaySize : MonoBehaviour {

    private int currentWidth;
    private int currentHeight;
    private bool isFullscreen;

    // Use this for initialization
    void Start() {
        currentHeight = Screen.currentResolution.height;
        currentWidth = Screen.currentResolution.width;
        isFullscreen = Screen.fullScreen;

        GameSettings settings = FindObjectOfType<GameSettings>();
        ReloadSettings(settings.getCurrentSettings());
        settings.OnSettingsChanged += ReloadSettings;
    }


    public bool isAllowedDisplaySetting(int width, int height, int refreshRate) {
        foreach (var res in Screen.resolutions) {
            if (res.height == height && res.width == width && res.refreshRate == refreshRate)
                return true;
        }
        return false;
    }

    public void ReloadSettings(GameSettings.Settings settings) {
        if (currentHeight != settings.height || currentWidth != settings.width || isFullscreen != settings.fullscreen) {
            if (isAllowedDisplaySetting(settings.width, settings.height, settings.refreshRate))
                Screen.SetResolution(settings.width, settings.height, settings.fullscreen, settings.refreshRate);
            else {
                Resolution fallback = Screen.resolutions[0];
                Screen.SetResolution(fallback.width, fallback.height, settings.fullscreen, fallback.refreshRate);
            }

            currentWidth = settings.width;
            currentHeight = settings.height;
            isFullscreen = settings.fullscreen;
        }
    }
}
