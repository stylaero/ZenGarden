﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/*
 * Options Menu system
 * From Skeletal Dance Simulator
 * By Jesper Tingvall 2016
 */
public class OptionsMenu : MonoBehaviour {
  public Dropdown qualitySetting;
  public Dropdown SSAOSetting;
  public Slider musicVolume;
  public Slider effectVolume;
  public Dropdown resolutionSetting;
  public Toggle isFullscreen;
  public InputField comPort;

  public InputField acceleration;
  public InputField orientation;
  public InputField gain;
  public InputField rejection;

  private GameSettings settings;
  private Resolution[] resolutions;

  void Awake() {
    settings = FindObjectOfType<GameSettings>();
    //settings.OnSettingsChanged += RebuildGUI;  // Avoid this - creates self-loop?
    RebuildGUI(settings.getCurrentSettings());

    qualitySetting.onValueChanged.AddListener(SetQualityLevel);
    SSAOSetting.onValueChanged.AddListener(SetSSAOSetting);
    resolutionSetting.onValueChanged.AddListener(SetResolution);
    isFullscreen.onValueChanged.AddListener(SetFullscreen);
  }


  public void RebuildGUI(GameSettings.Settings settings) {
    BuildQualitySettings();
    qualitySetting.value = settings.QualityLevel;

    SSAOSetting.value = settings.SSAOLevel;
    musicVolume.value = settings.musicVolume;
    effectVolume.value = settings.effectVolume;
    isFullscreen.isOn = settings.fullscreen;
    comPort.text = settings.comPort;

    acceleration.text = settings.wand3dAcceleration.ToString();
    orientation.text = settings.wand3dOrientation.ToString();
    gain.text = settings.wand3dGain.ToString();
    rejection.text = settings.outlinerFactor.ToString();

    BuildResolutionSettings();
    resolutionSetting.value = getSelectedResolution(settings);
  }


  private string getResolutionName(Resolution r) {
    return "" + r.width + " x " + r.height + " " + r.refreshRate + " Hz";
  }


  public void BuildResolutionSettings() {
    resolutions = Screen.resolutions;

    List<Dropdown.OptionData> resolutionsOptions = new List<Dropdown.OptionData>();
    foreach (Resolution res in resolutions) {
      resolutionsOptions.Add(new Dropdown.OptionData(getResolutionName(res)));
    }
    resolutionSetting.options = resolutionsOptions;
  }

  public void BuildQualitySettings() {
    List<Dropdown.OptionData> qualityOptions = new List<Dropdown.OptionData>();
    foreach (var q in QualitySettings.names)
      qualityOptions.Add(new Dropdown.OptionData(q));
    qualitySetting.options = qualityOptions;
  }

  private int getSelectedResolution(GameSettings.Settings settings) {
    for (int i = 0; i < resolutions.Length; i++) {
      Resolution r = resolutions[i];
      if (r.width == settings.width && r.height == settings.height && r.refreshRate == settings.refreshRate)
        return i;
    }
    return 0;
  }

  public void SetMusicVolume() {
    settings.setMusicVolume(musicVolume.value);
  }

  public void SetFullscreen(bool f) {
    settings.setFullscreen(f);
  }

  public void SetEffectVolume() {
    settings.setEffectVolume(effectVolume.value);
  }

  public void SetQualityLevel(int level) {
    settings.setQualityLevel(level);
  }

  public void SetSSAOSetting(int level) {
    settings.setSSAOLevel(level);
  }

  public void SetComPort(string port) {
    settings.setComPort(port);
  }

  public void SetResolution(int level) {
    Resolution selected = resolutions[level];
    settings.setResolution(selected.width, selected.height, selected.refreshRate);
  }

  public void UpdateTrackingParameters() {
    float acc = float.Parse(acceleration.text);
    float or = float.Parse(orientation.text);
    float gn = float.Parse(gain.text);
    float rj = float.Parse(rejection.text);
    settings.setTrackingParameters(acc, or, gn, rj);
  }
}