﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class VolumeLevels : MonoBehaviour {
    
    public AudioMixer masterMixer;

    private string masterChannel = "MasterVolume";
    private string EffectChannel = "EffectVolume";
    private string MusicChannel = "MusicVolume";

    // Use this for initialization
    void Start() {
        GameSettings settings = FindObjectOfType<GameSettings>();
        ReloadSettings(settings.getCurrentSettings());
        settings.OnSettingsChanged += ReloadSettings;
    }


    public void ReloadSettings(GameSettings.Settings settings) {
        masterMixer.SetFloat(MusicChannel, settings.musicVolume);
        masterMixer.SetFloat(EffectChannel, settings.effectVolume);
    }
}
