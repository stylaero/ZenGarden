﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class CameraGraphicQuality : MonoBehaviour {

  private ScreenSpaceAmbientOcclusion screenSpaceAmbientOcclusion;

  void Start() {
    screenSpaceAmbientOcclusion = GetComponent<ScreenSpaceAmbientOcclusion>();

    GameSettings settings = FindObjectOfType<GameSettings>();
    ReloadSettings(settings.getCurrentSettings());
    settings.OnSettingsChanged += ReloadSettings;
  }


  public void ReloadSettings(GameSettings.Settings settings) {
    SetSSAO(settings.SSAOLevel);
  }


  private ScreenSpaceAmbientOcclusion.SSAOSamples intToSSAOLevel(int level) {
    switch (level) {
      case 1: return ScreenSpaceAmbientOcclusion.SSAOSamples.Low;
      case 2: return ScreenSpaceAmbientOcclusion.SSAOSamples.Medium;
      case 3: return ScreenSpaceAmbientOcclusion.SSAOSamples.High;
      default: throw new System.NotImplementedException();
    }
  }


  public void SetSSAO(int level) {
    if (level == 0) {
      screenSpaceAmbientOcclusion.enabled = false;
    } else {
      screenSpaceAmbientOcclusion.enabled = true;
      screenSpaceAmbientOcclusion.m_SampleCount = intToSSAOLevel(level);
    }
  }
}
