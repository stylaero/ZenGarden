﻿using UnityEngine;
using Stylaero.Wand3D;

/*
 * Options Menu system
 * From Skeletal Dance Simulator
 * By Jesper Tingvall 2016
 */
public class GameSettings : MonoBehaviour {

  public class Settings {
    public int SSAOLevel;
    public float musicVolume;
    public float effectVolume;

    public string comPort;

    public float wand3dAcceleration = 14;
    public float wand3dOrientation = 14;
    public float wand3dGain = 0.000000006f;
    public double outlinerFactor = 25 * 25;

    public bool fullscreen {
      get {
        return Screen.fullScreen;
      }
    }

    public int width {
      get {
        return Screen.width;
      }
    }

    public int height {
      get {
        return Screen.height;
      }
    }

    public int refreshRate {
      get {
        return Screen.currentResolution.refreshRate;
      }
    }

    public int QualityLevel {
      get {
        return QualitySettings.GetQualityLevel();
      }
    }

    public Settings() {
      if (PlayerPrefs.HasKey("SSAOLevel")) SSAOLevel = PlayerPrefs.GetInt("SSAOLevel");
      if (PlayerPrefs.HasKey("musicVolume")) musicVolume = PlayerPrefs.GetFloat("musicVolume");
      if (PlayerPrefs.HasKey("effectVolume")) effectVolume = PlayerPrefs.GetFloat("effectVolume");

      comPort = Wand3D.DefaultPort;
      if (PlayerPrefs.HasKey("comPort")) comPort = PlayerPrefs.GetString("comPort");

      if (PlayerPrefs.HasKey("wand3dAcceleration")) wand3dAcceleration = PlayerPrefs.GetFloat("wand3dAcceleration");
      if (PlayerPrefs.HasKey("wand3dOrientation")) wand3dOrientation = PlayerPrefs.GetFloat("wand3dOrientation");
      if (PlayerPrefs.HasKey("wand3dGain")) wand3dGain = PlayerPrefs.GetFloat("wand3dGain");
      if (PlayerPrefs.HasKey("wand3dOutlinerFactor")) outlinerFactor = PlayerPrefs.GetFloat("wand3dOutlinerFactor");
    }

    public void SaveSettings() {
      PlayerPrefs.SetInt("SSAOLevel", SSAOLevel);
      PlayerPrefs.SetFloat("musicVolume", musicVolume);
      PlayerPrefs.SetFloat("effectVolume", effectVolume);
      PlayerPrefs.SetString("comPort", comPort);

      PlayerPrefs.SetFloat("wand3dAcceleration", wand3dAcceleration);
      PlayerPrefs.SetFloat("wand3dOrientation", wand3dOrientation);
      PlayerPrefs.SetFloat("wand3dGain", wand3dGain);
      PlayerPrefs.SetFloat("wand3dOutlinerFactor", (float)outlinerFactor);
    }
  }

  public delegate void SettingsChanged(Settings settings);
  public event SettingsChanged OnSettingsChanged;

  private Settings currentSettings;

  private void notifySettingsChanged() {
    currentSettings.SaveSettings();
    if (OnSettingsChanged != null)
      OnSettingsChanged(currentSettings);
  }

  public Settings LoadSettings() {
    return new Settings();
  }

  public Settings getCurrentSettings() {
    if (currentSettings == null) {
      currentSettings = LoadSettings();
      QualitySettings.SetQualityLevel(currentSettings.QualityLevel, true);
    }
    return currentSettings;
  }

  public void setResolution(int width, int height, int refreshRate, bool fullscreen) {
    Screen.SetResolution(width, height, fullscreen, refreshRate);
    notifySettingsChanged();
  }

  public void setResolution(int width, int height, int refreshRate) {
    Settings settings = getCurrentSettings();
    setResolution(width, height, refreshRate, settings.fullscreen);
  }

  public void setFullscreen(bool fullscreen) {
    Settings settings = getCurrentSettings();
    setResolution(settings.width, settings.height, settings.refreshRate, fullscreen);
  }

  public void setMusicVolume(float musicLevel) {
    getCurrentSettings().musicVolume = musicLevel;
    notifySettingsChanged();
  }

  public void setEffectVolume(float level) {
    getCurrentSettings().effectVolume = level;
    notifySettingsChanged();
  }

  public void setSSAOLevel(int level) {
    getCurrentSettings().SSAOLevel = level;
    notifySettingsChanged();
  }

  public void setQualityLevel(int level) {
    QualitySettings.SetQualityLevel(level, true);
    notifySettingsChanged();
  }

  public void setComPort(string port) {
    getCurrentSettings().comPort = port;
    notifySettingsChanged();
  }

  public void setTrackingParameters(float acceleration, float orientation, float gain, double outlinerRejection) {
    getCurrentSettings().wand3dAcceleration = acceleration;
    getCurrentSettings().wand3dOrientation = orientation;
    getCurrentSettings().wand3dGain = gain;
    getCurrentSettings().outlinerFactor = outlinerRejection;
    notifySettingsChanged();
  }
}