﻿using UnityEngine;
using System.Collections;
using System;

public class WindAffector : MonoBehaviour, IWindAffector {
  private WindField wind;
  private Rigidbody body;

  public float size;

  public Texture2D forceTexture;
  public Vector2 textureScale;

  public Transform windCenter;

	void Awake () {
    wind = FindObjectOfType<WindField>();
    body = GetComponentInParent<Rigidbody>();
	}

  void OnEnable() {
    wind.AddWindAffector(this);
  }

  void OnDisable() {
    wind.RemoveWindAffector(this);
  }

  public Vector3 GetForce(Vector3 p) {
    Vector3 forcePos = windCenter.position;
    Vector3 forceSize = body.GetPointVelocity(forcePos);

    Vector3 y;
    Vector3 x;
    BuildLocalBase(forcePos, forceSize, p, out x, out y);

    Vector2 localPos = GetTexturePosition(LocalPosition(forcePos, forceSize, p, x, y));

    if (Mathf.Abs(localPos.x) > textureScale.x/2.0f || Mathf.Abs(localPos.y) > textureScale.y/2.0f) {
      return Vector3.zero;
    }

    Color pixel = forceTexture.GetPixelBilinear(localPos.x, localPos.y);
    Vector3 force = Force2DTo3D(ColorToForce(pixel), x, y) * forceSize.magnitude;
    Debug.DrawRay(p, force, Color.blue);

    return force * size;
  }

  private Vector2 GetTexturePosition(Vector2 pos) {
    return new Vector2((float)(pos.x + 0.5) / textureScale.x, (float)(pos.y + 0.5) / textureScale.y);
  }

  private Vector3 Force2DTo3D(Vector2 force, Vector3 x, Vector3 y) {
    return x * force.x + y * force.y;
  }

  private Vector2 ColorToForce(Color c) {
    return new Vector2(c.g - 0.5f, c.r - 0.5f);
  }

  private void BuildLocalBase(Vector3 affector, Vector3 affectorVelocity, Vector3 affected, out Vector3 x, out Vector3 y) {
    Vector3 d = (-affector + affected);
    Vector3 projected = Vector3.Project(d, affectorVelocity);
    y = (d - projected).normalized;
    x = affectorVelocity.normalized;
  }

  private Vector2 LocalPosition(Vector3 affector, Vector3 affectorVelocity, Vector3 affected, Vector3 x, Vector3 y) {
    Vector3 d = (-affector + affected);
    Vector2 planeCoord = new Vector2(Vector3.Dot(affectorVelocity.normalized, d), Vector3.Dot(y, d));
    return planeCoord;
  }
}