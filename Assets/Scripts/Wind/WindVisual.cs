﻿using UnityEngine;
using System.Collections;

public class WindVisual : MonoBehaviour {

  public Rigidbody body;
	
	void Update () {
    Vector3 towardsCamera = transform.position - Camera.main.transform.position;
    transform.rotation = Quaternion.LookRotation(body.velocity, towardsCamera);
	}
}
