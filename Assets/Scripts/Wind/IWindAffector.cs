﻿using UnityEngine;

public interface IWindAffector {
  Vector3 GetForce(Vector3 p); 
}