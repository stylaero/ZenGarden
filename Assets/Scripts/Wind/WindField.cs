﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindField : MonoBehaviour {

  private List<IWindAffector> windAffectors = new List<IWindAffector>();
  private static float maxForce = 900;

  public void AddWindAffector(IWindAffector wind) {
    windAffectors.Add(wind);
  }

  public void RemoveWindAffector(IWindAffector wind) {
    windAffectors.Remove(wind);
  }

  public Vector3 GetForce(Vector3 p) {
    Vector3 force = Vector3.zero;
    foreach(var affector in windAffectors) {
      force += affector.GetForce(p);
    }
    return Vector3.ClampMagnitude(force, maxForce);
  }
}