﻿using UnityEngine;
using System.Collections;

public class WindAffected : MonoBehaviour {
  private WindField wind;
  private Rigidbody body;

  private BoxCollider collider;

	void Start () {
    wind = FindObjectOfType<WindField>();
    body = GetComponent<Rigidbody>();
    collider = GetComponent<BoxCollider>();
	}

  private Vector3 sampleRandomPoint() {
    Vector3 range = collider.bounds.max;
    Vector3 sample = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y), Random.Range(-range.z, range.z));
    return collider.ClosestPointOnBounds(sample + transform.position);
  }
	
	void FixedUpdate () {
    Vector3 sample = sampleRandomPoint();
    body.AddForceAtPosition(wind.GetForce(sample), sample, ForceMode.Force);
	}
}
