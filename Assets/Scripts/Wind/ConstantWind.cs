﻿using UnityEngine;
using System.Collections;

public class ConstantWind : MonoBehaviour, IWindAffector {
  private WindField wind;

  public Vector3 ConstantWindField;

	void Awake () {
    wind = FindObjectOfType<WindField>();
	}

  void OnEnable() {
    wind.AddWindAffector(this);
  }

  void OnDisable() {
    wind.RemoveWindAffector(this);
  }

  public Vector3 GetForce(Vector3 p) {
    return ConstantWindField;
  }
}
