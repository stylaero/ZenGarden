﻿using UnityEngine;
using System.Collections;

public class LevelScript : MonoBehaviour {
  private GameManager gameManager;
  private IPuzzle[] leafPiles;
  private bool levelCompleted;

  private bool levelPlaying;

  public float StartTime;

  public delegate void VoidDelegate();

  public event VoidDelegate OnLevelCompleted;
  public event VoidDelegate OnLevelStart;

  public bool HasLeafs;
  public bool HighCamera;

  private LeafSpawner leafs;

	void Awake () {
    gameManager = FindObjectOfType<GameManager>();
    leafPiles = GetComponentsInChildren<IPuzzle>();
    leafs = FindObjectOfType<LeafSpawner>();
    foreach (var l in leafPiles) {
      l.OnPuzzleSolved += CheckCompletion;
    }
	}

  public void PrepareLevel() {
    gameObject.SetActive(true);
    levelCompleted = false;
    levelPlaying = false;
    if (HasLeafs) {
      leafs.EnableLeafs();
    } else {
      leafs.DisableLeafs();
    }
    Invoke("StartLevel", StartTime);
    gameManager.SetHighCamera(HighCamera);
  }

  public void StartLevel() {
    levelPlaying = true;
    if (OnLevelStart != null)
      OnLevelStart();
  }
	
	public void CheckCompletion() {
    if (!levelPlaying)
      return;

    bool AllSucceeded = true;
    foreach (var l in leafPiles) {
      if (!l.Solved()) {
        AllSucceeded = false;
        break;
      }
    }

    if (AllSucceeded && !levelCompleted) {
      if (OnLevelCompleted != null)
        OnLevelCompleted();
      NextLevel();
    }
	}

  public void NextLevel() {
    if (levelCompleted)
      return;
    levelCompleted = true;
    gameManager.LevelFinished(this);
  }

  public void CleanUpLevel() {
    gameObject.SetActive(false);
    levelCompleted = false;
    levelPlaying = false;
  }
}