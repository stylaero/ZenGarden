﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InitializedToggle : MonoBehaviour {

  private Wand3DCursor cursor;
  private Toggle toggle;

	void Start () {
    cursor = FindObjectOfType<Wand3DCursor>();
    toggle = GetComponent<Toggle>();
	}
	
	void FixedUpdate () {
    toggle.isOn = cursor.Wand.IsInitialized();
	}
}
