﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageScript : MonoBehaviour {

  private static string FadeIn = "FadeIn";
  private static string FadeOut = "FadeOut";

  private Text text;
  private Animator animator;

	void Awake () {
    text = GetComponent<Text>();
    animator = GetComponent<Animator>();
	}
	
	public void ShowText (string message) {
    text.text = message;
    animator.SetTrigger(FadeIn);
	}

  public void HideText() {
    animator.SetTrigger(FadeOut);
  }
}
