﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComPortInputField : MonoBehaviour {
  private GameSettings gameSettings;
  private InputField input;

	void Start () {
    gameSettings = FindObjectOfType<GameSettings>();
    input = GetComponent<InputField>();
    gameSettings.OnSettingsChanged += CheckNewComPort;
	}
	
	void CheckNewComPort (GameSettings.Settings settings) {
    if (input.text.CompareTo(settings.comPort) != 0)
      input.text = settings.comPort;
	}
}
