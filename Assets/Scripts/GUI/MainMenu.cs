﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
  private string FadeOut = "FadeOut";
  private string FadeIn = "FadeIn";
  private string Cancel = "Cancel";

  private bool menuVisible = true;

  private Animator animator;

  private GameManager gameManager;
  private MessageScript message;

	void Start () {
    animator = GetComponent<Animator>();
    gameManager = FindObjectOfType<GameManager>();
    message = FindObjectOfType<MessageScript>();
	}
	
  public void StartGame() {
    gameManager.StartGame();
    animator.SetTrigger(FadeOut);
    menuVisible = false;
  }

  public void ExitGame() {
    Application.Quit();
  }

  public void ShowMenu() {
    message.HideText();
    animator.SetTrigger(FadeIn);
    menuVisible = true;
  }

  void Update() {
    if (Input.GetButtonDown(Cancel) && !menuVisible) {
      ShowMenu();
    }
  }

  public void MenuHasFadeIn() {
    gameManager.ResetLevels();
  }
}