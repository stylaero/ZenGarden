﻿using UnityEngine;
using System.Collections;
using System;

public class CalibrateWandDevicePuzzle : MonoBehaviour, IPuzzle {

  private Wand3DCursor cursor;

  private LevelScript level;
  private MessageScript message;

  public string MoveAway;
  public string Calibrating;
  public string CalibrationDone;

  public event PuzzleSolved OnPuzzleSolved;

  void Awake() {
    cursor = FindObjectOfType<Wand3DCursor>();
    level = GetComponent<LevelScript>();
    message = FindObjectOfType<MessageScript>();
	}

  void OnEnable() {
    StartCoroutine("Calibrate");
  }

  IEnumerator Calibrate() {
    yield return new WaitForSeconds(4);
    message.ShowText(MoveAway);
    yield return new WaitForSeconds(4);
    message.HideText();

    yield return new WaitForSeconds(1);
    message.ShowText(Calibrating);
    yield return new WaitForSeconds(1);
    cursor.Calibrate();

    while (!cursor.Wand.IsCalibrated())
      yield return null;

    message.HideText();
    yield return new WaitForSeconds(1);
    message.ShowText(CalibrationDone);
    yield return new WaitForSeconds(1);
    message.HideText();

    OnPuzzleSolved();
  }

  public bool Solved() {
    return cursor.Wand.IsCalibrated();
  }
}