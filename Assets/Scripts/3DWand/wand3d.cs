﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;


// Namespace rules: CompanyName.TechnologyName[.Feature][.Design]
// https://msdn.microsoft.com/en-us/library/893ke618(v=vs.71).aspx

namespace Stylaero.Wand3D {

  public class Wand3DException : Exception {
    public ErrorCode error;

    public Wand3DException(ErrorCode e) {
      error = e;
    }

    public override string ToString() {
      return error.ToString();
    }
  }

  public class Wand3DSerialException : Wand3DException {
    public Wand3DSerialException(ErrorCode e) : base(e) { }
  }

  public class Wand3DFailException : Wand3DException {
    public Wand3DFailException(ErrorCode e) : base(e) { }
  }

  public class Wand3DNotInitializedException : Wand3DException {
    public Wand3DNotInitializedException(ErrorCode e) : base(e) { }
  }

  public enum ErrorCode {
    WAND3D_SUCCESS = 0,
    WAND3D_FAIL = 1000,
    WAND3D_SERIAL_EXCEPTION = 2000,
    WAND3D_NOT_INITIALIZED = 3000,
    WAND3D_UNKNOWN_ERROR = 9000
  };

  //[StructLayout(LayoutKind.Sequential, Size = 9), Serializable]
  [StructLayout(LayoutKind.Sequential)]
  public struct WandDataInternal {
    [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3)]
    public double[] position;

    [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3)]
    public double[] velocity;

    [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 3)]
    public double[] orientation;
  };


  public class WandData {
    private WandDataInternal data;

    public WandData(WandDataInternal data) {
      this.data = data;
    }

    public Vector3 Position {
      get {
        return new Vector3((float)data.position[0], (float)data.position[2], (float)data.position[1]);
      }
    }

    public Vector3 Velocity {
      get {
        return new Vector3((float)data.velocity[0], (float)data.velocity[2], (float)data.velocity[1]);
      }
    }

    public Vector3 Orientation {
      get {
        return new Vector3((float)data.orientation[0], (float)data.orientation[2], (float)data.orientation[1]);
      }
    }

    public Quaternion Rotation {
      get {
        Vector3 angles = new Vector3((float)data.orientation[0], (float)data.orientation[2], (float)data.orientation[1]);
        if (angles.magnitude == 0)
          return Quaternion.identity;
        return Quaternion.LookRotation(angles, Vector3.up);
      }
    }
  }

  public class Wand3D : IDisposable {

    public static string DefaultPort {
      get {
        switch (Application.platform) {
          case RuntimePlatform.WindowsEditor:
          case RuntimePlatform.WindowsPlayer:
          return "COM3";
          case RuntimePlatform.OSXEditor:
          case RuntimePlatform.OSXPlayer:
          case RuntimePlatform.OSXDashboardPlayer:
          return "/dev/tty.usbmodem12345671";
        }
        throw new NotImplementedException();
      }
    }

    public static string WrapperVersion = "0.6";

    public static string LibraryVersion {
      get {
        return GetLibraryVersion();
      }
    }

    private IntPtr wand3D;

    [DllImport("wand3d")]
    private static extern ErrorCode getLibraryVersion(StringBuilder name);

    [DllImport("wand3d")]
    private static extern ErrorCode CreateWand3dDevice(out IntPtr device, StringBuilder devname);

    [DllImport("wand3d")]
    private static extern ErrorCode DestroyWand3dDevice(IntPtr wand3dDevice);

    [DllImport("wand3d")]
    private static extern ErrorCode stopWand3dDevice(IntPtr wand3dDevice);

    [DllImport("wand3d")]
    private static extern ErrorCode startWand3dDevice(IntPtr wand3dDevice);

    [DllImport("wand3d")]
    private static extern ErrorCode isInitialized(IntPtr wand3dDevice, out bool initialized);

    [DllImport("wand3d")]
    private static extern ErrorCode getData(IntPtr wand3dDevice, out WandDataInternal data);

    [DllImport("wand3d")]
    private static extern ErrorCode setInput(IntPtr wand3dDevice, StringBuilder name);

    [DllImport("wand3d")]
    private static extern ErrorCode isSerialInput(IntPtr wand3dDevice, out bool serial);

    [DllImport("wand3d")]
    private static extern ErrorCode reCalibrate(IntPtr wand3dDevice);

    [DllImport("wand3d")]
    private static extern ErrorCode getNetwork(IntPtr wand3dDevice, out IntPtr network);

    [DllImport("wand3d")]
    private static extern ErrorCode isCalibrated(IntPtr wand3dDevice, out bool calibrated);

    [DllImport("wand3d")]
    private static extern ErrorCode getStaticField(IntPtr wand3dDevice, double[] field);

    [DllImport("wand3d")]
    private static extern ErrorCode setStaticField(IntPtr wand3dDevice, double[] field);

    [DllImport("wand3d")]
    private static extern ErrorCode getRawData(IntPtr wand3dDevice, int[] measurement);

    [DllImport("wand3d")]
    private static extern ErrorCode getSensorStatus(IntPtr wand3dDevice, int[] statusArray, out int statusArraySize);

    [DllImport("wand3d")]
    private static extern ErrorCode setStandardDeviations(IntPtr wand3dDevice, double acceleration, double orientation, double gain);

    [DllImport("wand3d")]
    private static extern ErrorCode setOutlinerRejectionFactor(IntPtr wand3dDevice, double outlinerFactor);

    [DllImport("wand3d")]
    private static extern ErrorCode getInput(IntPtr wand3dDevice, StringBuilder name);

    public static void CheckForError(ErrorCode e) {
      switch (e) {
        case ErrorCode.WAND3D_SUCCESS:
        return;
        case ErrorCode.WAND3D_SERIAL_EXCEPTION:
        throw new Wand3DSerialException(e);
        case ErrorCode.WAND3D_FAIL:
        throw new Wand3DFailException(e);
        case ErrorCode.WAND3D_NOT_INITIALIZED:
        throw new Wand3DNotInitializedException(e);
        default:
        throw new Wand3DException(e);
      }
    }

    public static bool DidSucceed(ErrorCode e) {
      switch (e) {
        case ErrorCode.WAND3D_SUCCESS:
        return true;
        case ErrorCode.WAND3D_FAIL:
        return false;
        default:
        CheckForError(e);
        break;
      }
      return false; // Never executes
    }


    public Wand3D(string port) {
      StringBuilder cString = new StringBuilder(port);
      ErrorCode success = CreateWand3dDevice(out wand3D, cString);
      CheckForError(success);
    }


    public void StopWand3DDevice() {
      stopWand3dDevice(wand3D);
    }

    private static string GetLibraryVersion() {
      int bufferSize = 512;
      StringBuilder buffer = new StringBuilder(bufferSize);
      CheckForError(getLibraryVersion(buffer));
      return buffer.ToString();
    }


    public bool StartWand3dDevice() {
      return DidSucceed(startWand3dDevice(wand3D));
    }


    public bool[] GetSensorStatus() {
      int size = 12;
      int[] status = new int[size];
      CheckForError(getSensorStatus(wand3D, status, out size));

      bool[] statusArray = new bool[size];

      for (int i = 0; i < size; i++) {
        if (status[i] == 0)
          statusArray[i] = true;
        else
          statusArray[i] = false;
      }
      return statusArray;
    }


    public bool IsInitialized() {
      bool initialized;
      CheckForError(isInitialized(wand3D, out initialized));
      return initialized;
    }


    public WandData GetData() {
      WandDataInternal data;
      CheckForError(getData(wand3D, out data));
      return new WandData(data);
    }


    public bool setInput(string name) {
      StringBuilder cString = new StringBuilder(name);
      return DidSucceed(setInput(wand3D, cString));
    }


    public string GetInput() {
      int bufferSize = 512;
      StringBuilder buffer = new StringBuilder(bufferSize);
      CheckForError(getInput(wand3D, buffer));
      return buffer.ToString();
    }


    public bool IsSerialInput() {
      bool serial;
      CheckForError(isSerialInput(wand3D, out serial));
      return serial;
    }


    public void ReCalibrate() {
      CheckForError(reCalibrate(wand3D));
    }


    public Network GetNetwork() {
      IntPtr networkPtr;
      CheckForError(getNetwork(wand3D, out networkPtr));
      return new Network(networkPtr);
    }


    public bool IsCalibrated() {
      bool calibrated;
      CheckForError(isCalibrated(wand3D, out calibrated));
      return calibrated;
    }


    public double[] GetStaticField() {
      double[] field = new double[12];
      CheckForError(getStaticField(wand3D, field));
      return field;
    }


    public Vector3 GetStaticField(int sensor) {
      double[] field = GetStaticField();
      int[] indexes = { sensor * 3, sensor * 3 + 1, sensor * 3 + 2 };
      return new Vector3((float)field[indexes[0]], (float)field[indexes[2]], (float)field[indexes[1]]);
    }


    public void SetStaticField(double[] field) {
      CheckForError(setStaticField(wand3D, field));
    }


    public void setStandardDeviations(double acceleration, double orientation, double gain) {
      CheckForError(setStandardDeviations(wand3D, acceleration, orientation, gain));
    }


    public void setOutlinerRejectionFactor(double outlinerFactor) {
      CheckForError(setOutlinerRejectionFactor(wand3D, outlinerFactor));
    }


    public bool GetRawData(int[] measurement) {
      return DidSucceed(getRawData(wand3D, measurement));
    }



    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing) {
      if (!disposedValue) {
        if (disposing) {
          // TODO: dispose managed state (managed objects).
        }

        // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
        // TODO: set large fields to null.
        DestroyWand3dDevice(wand3D);
        wand3D = IntPtr.Zero;

        disposedValue = true;
      }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    ~Wand3D() {
      //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      Dispose(false);
    }

    // This code added to correctly implement the disposable pattern.
    public void Dispose() {
      // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
      Dispose(true);
      // TODO: uncomment the following line if the finalizer is overridden above.
      // GC.SuppressFinalize(this);
    }
    #endregion
  }
}
