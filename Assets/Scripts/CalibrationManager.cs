﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CalibrationManager : MonoBehaviour {

  private static string FadeIn = "FadeIn";
  private static string FadeOut = "FadeOut";
  private static string CalibrateButton = "Calibrate";

  private Text text;
  private Animator animator;
  private Wand3DCursor cursor;

  public string MoveAway;
  public string Calibrating;
  public string CalibrationDone;

	void Awake () {
    text = GetComponent<Text>();
    animator = GetComponent<Animator>();
    cursor = FindObjectOfType<Wand3DCursor>();
	}

  void Update() {
    if (Input.GetButtonDown(CalibrateButton)) {
      StartCalibrate();
    }
  }

  IEnumerator Calibrate() {
    ShowText(MoveAway);
    yield return new WaitForSeconds(2);
    HideText();

    yield return new WaitForSeconds(1);
    ShowText(Calibrating);
    cursor.Calibrate();

    while (!cursor.Wand.IsCalibrated())
      yield return null;

    HideText();
    yield return new WaitForSeconds(1);
    ShowText(CalibrationDone);
    yield return new WaitForSeconds(1);
    HideText();
  }


  public void StartCalibrate() {
    StartCoroutine("Calibrate");
  }
	
	public void ShowText (string message) {
    text.text = message;
    animator.SetTrigger(FadeIn);
	}

  public void HideText() {
    animator.SetTrigger(FadeOut);
  }
}
