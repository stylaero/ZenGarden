﻿using UnityEngine;
using System.Collections;

public class CalibrateScript : MonoBehaviour {

  public GameObject broomModel;
  public GameObject wand3dRoot;
  public GameObject calibrateGoal;

	void Awake() {
    GetComponent<LevelScript>().OnLevelCompleted += Hide;
    GetComponent<LevelScript>().OnLevelStart += Enable;
	}
	
	void FixedUpdate () {
    bool flip = wand3dRoot.transform.eulerAngles.x < 0 || wand3dRoot.transform.eulerAngles.x > 90;

    if (flip) {
      broomModel.transform.localEulerAngles = new Vector3(90, 0, 0);
    } else {
      broomModel.transform.localEulerAngles = new Vector3(270, 0, 0);
    }

    Vector3 localPos = broomModel.transform.localPosition;
    broomModel.transform.position = calibrateGoal.transform.position;
    float z = broomModel.transform.localPosition.z;
    localPos.z = z;
    broomModel.transform.localPosition = localPos;
	}

  public void Enable() {
    enabled = true;
  }

  public void Hide() {
    enabled = false;
  }
}
