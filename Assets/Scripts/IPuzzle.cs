﻿public delegate void PuzzleSolved();

public interface IPuzzle {
  event PuzzleSolved OnPuzzleSolved;

  bool Solved();
}