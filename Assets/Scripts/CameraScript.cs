﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
  private GameManager gameManager;

	void Start () {
    gameManager = FindObjectOfType<GameManager>();
	}
	
	public void CameraMovedOut() {
    gameManager.CameraMovedOut();
	}
}
