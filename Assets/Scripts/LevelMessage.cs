﻿using UnityEngine;
using System.Collections;

public class LevelMessage : MonoBehaviour {

  private LevelScript level;
  private MessageScript message;

  public string MessageText;

	void Awake() {
    level = GetComponent<LevelScript>();
    message = FindObjectOfType<MessageScript>();
    level.OnLevelCompleted += HideMessage;
	}

  void OnEnable() {
    message.ShowText(MessageText);
  }

  public void HideMessage() {
    message.HideText();
  }
}
