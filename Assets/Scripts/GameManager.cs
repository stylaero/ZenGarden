﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
  public static string CameraIn = "CameraIn";
  public static string CameraOut = "CameraOut";
  public static string HighCamera = "HighCamera";

  public Animator CameraAnimator;

  public int currentLevel = 0;
  public int nextLevel;

  public LevelScript[] Levels;
  private LeafSpawner leafSpawner;

  private MessageScript message;

  void Start () {
    leafSpawner = FindObjectOfType<LeafSpawner>();
    message = FindObjectOfType<MessageScript>();
	}

  public void LevelFinished(LevelScript level) {
    nextLevel = currentLevel+1;
    CameraAnimator.SetTrigger(CameraOut);
  }

  public void SetHighCamera(bool high) {
    CameraAnimator.SetBool(HighCamera, high);
  }

  public void CameraMovedOut() {
    Levels[currentLevel].CleanUpLevel();
    currentLevel = nextLevel;
    if (currentLevel == Levels.Length) {
      // Show win menu here
      SceneManager.LoadScene(0);
    } else {
      Levels[currentLevel].PrepareLevel();
      CameraAnimator.SetTrigger(CameraIn);
    }
  }

  public void StartGame() {
    ResetLevels();
    Levels[0].PrepareLevel();
    CameraAnimator.SetTrigger(CameraIn);
  }

  public void ResetLevels() {
    currentLevel = 0;
    foreach (var l in Levels) {
      l.CleanUpLevel();
    }
  }

  public void SwitchToLevel(int level) {
    nextLevel = level;
    message.HideText();
    CameraAnimator.SetTrigger(CameraOut);
  }
}