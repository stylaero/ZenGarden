﻿using UnityEngine;
using UnityEngine.UI;

using Stylaero.Wand3D;

public class Wand3DCursor : MonoBehaviour {
  public Wand3D Wand;

  private Rigidbody body;
  private GameSettings gameSettings;

  public float deviceScale;

  public Vector3 DeviceRotation;

  void Start() {
    gameSettings = FindObjectOfType<GameSettings>();

    body = GetComponent<Rigidbody>();
    DestroyWand();
    Wand = new Wand3D(gameSettings.getCurrentSettings().comPort);
    Wand.StartWand3dDevice();
    var settings = gameSettings.getCurrentSettings();
    Wand.setOutlinerRejectionFactor(settings.outlinerFactor);
    Wand.setStandardDeviations(settings.wand3dAcceleration, settings.wand3dOrientation, settings.wand3dGain);

    gameSettings.OnSettingsChanged += ReloadSettings;
  }

  public void ReloadSettings(GameSettings.Settings settings) {
    string input = Wand.GetInput();
    //if (input.CompareTo(settings.comPort) != 0) {
    Wand.StopWand3DDevice();
    Wand.setInput(settings.comPort);
    Debug.Log(Wand.GetInput());
    Debug.Log(Wand.StartWand3dDevice());

    Wand.setOutlinerRejectionFactor(settings.outlinerFactor);
    Wand.setStandardDeviations(settings.wand3dAcceleration, settings.wand3dOrientation, settings.wand3dGain);
    //}
  }

  public void Calibrate() {
    Wand.ReCalibrate();
  }

  public void DestroyWand() {
    if (Wand != null) {
      Wand.Dispose();
      Wand = null;
    }
  }


  void Update() {
    if (Wand != null) {
      if (Wand.IsCalibrated()) {
        var data = Wand.GetData();
        Quaternion rot = Quaternion.Euler(DeviceRotation);
        body.MovePosition(rot * data.Position * deviceScale);
        body.MoveRotation(rot * data.Rotation);
        body.velocity = rot * data.Velocity * deviceScale;
      }
    }
  }

  void OnDestroy() {
    DestroyWand();
  }
}