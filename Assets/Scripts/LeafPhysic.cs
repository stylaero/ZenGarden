﻿using UnityEngine;
using System.Collections;

public class LeafPhysic : MonoBehaviour {

  private Rigidbody body;

  public float Area;

  public float AirDensity;

  private static float maxForce = 900;

	void Start () {
    body = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
    body.AddForce(GetAirForce(), ForceMode.Force);
	}

  // Assumes object is a flat plane
  private Vector3 GetAirForce() {
    float VolumeMoved = Area * Vector3.Dot(body.velocity, -transform.up);
    Vector3 force =  transform.up * AirDensity * VolumeMoved * body.velocity.magnitude;
    return Vector3.ClampMagnitude(force, maxForce);
  }
}
