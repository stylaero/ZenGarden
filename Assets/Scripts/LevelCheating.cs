﻿using UnityEngine;
using System.Collections;

public class LevelCheating : MonoBehaviour {

  private GameManager gameManager;

	void Start () {
    gameManager = GetComponent<GameManager>();
	}
	
	void Update () {
    for (int i = 0; i <= 9; i++) {
      if (Input.GetKeyDown("" + i)) {
        gameManager.SwitchToLevel(i);
        break;
      }
    }
  }
}